package pl.java.tripCRUD.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java.tripCRUD.model.Trip;
import pl.java.tripCRUD.repository.TripRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TripService {

    @Autowired
    private TripRepository tripRepository;

    public List<Trip> tripList(){
        return tripRepository.findAll();
    }

    public void save(Trip trip){
        tripRepository.save(trip);
    }

    public Trip get(long id){
        return tripRepository.findById(id).get();
    }

    public void delete(long id){
        tripRepository.deleteById(id);
    }
}
