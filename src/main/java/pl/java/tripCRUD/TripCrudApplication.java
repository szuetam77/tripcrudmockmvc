package pl.java.tripCRUD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TripCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(TripCrudApplication.class, args);
	}

}
