package pl.java.tripCRUD.controller;

import lombok.RequiredArgsConstructor;
import pl.java.tripCRUD.model.Trip;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.java.tripCRUD.repository.TripRepository;
import java.util.List;
import javax.validation.Valid;

@RestController
@RequestMapping("/trip")
@RequiredArgsConstructor
public class TripController {

    private final TripRepository tripRepository;

    @PostMapping("/save")
    public ResponseEntity<Trip> save(@RequestBody Trip trip){
        return new ResponseEntity<>(tripRepository.saveAndFlush(trip), HttpStatus.CREATED);
    }

    @GetMapping("/get")
    public ResponseEntity<List<Trip>> getAll(){
        return ResponseEntity.ok(tripRepository.findAll());
    }

    @GetMapping("get/{id}")
    public ResponseEntity<Trip> getTripById(@PathVariable("id") @Valid Long id){
        return ResponseEntity.ok(tripRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Unknown ID" + id)));
    }

    @DeleteMapping
    public ResponseEntity<Trip> deleteTripById (@PathVariable("id") @Valid Long id){
        if(!tripRepository.existsById(id)){
            return ResponseEntity.notFound().build();
        }
        tripRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("trip/{id}")
    public Trip editTripById(@RequestBody Trip newTrip, @PathVariable("id") @Valid Long id){
        return tripRepository.findById(id)
                .map(trip -> {
                    trip.setCountry(newTrip.getCountry());
                    trip.setCity(newTrip.getCity());
                    trip.setDuration(newTrip.getDuration());
                    return tripRepository.save(trip);
                })
                .orElseGet(() -> {
                    newTrip.setId(id);
                    return tripRepository.save(newTrip);
                });

    }
}
