package pl.java.tripCRUD.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import pl.java.tripCRUD.model.Trip;

public interface TripRepository extends JpaRepository<Trip, Long> {
}
