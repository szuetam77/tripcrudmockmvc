package pl.java.tripCRUD;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class TripCrudApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Test
	public void shouldReturnAllTrips() throws Exception{
		mvc.perform(MockMvcRequestBuilders.get("/get")
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.trip").exists())
				.andExpect(MockMvcResultMatchers.jsonPath("$.trip[*].tripId").isNotEmpty());
	}

	@Test
	public void shouldReturnTripById() throws Exception{
		mvc.perform(MockMvcRequestBuilders
				.get("get/{id}", 1)
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$tripId").value(1));
	}

//	@Test
//	public void shouldReturnSavedTrips() throws Exception{
//		mvc.perform(MockMvcRequestBuilders.post("/save")
//				.content(asJsonString(new Trip("Poland", "Warsaw", 1000)))
//				.contentType(MediaType.APPLICATION_JSON)
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isCreated())
//				.andExpect(MockMvcResultMatchers.jsonPath("$.tripId").exists());
//	}

	public static String asJsonString(final Object obj){
		try{
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e){
			throw new RuntimeException(e);
		}
	}

//	@Test
//	public void shouldReturnUpdatedTrip() throws Exception{
//		mvc.perform( MockMvcRequestBuilders
//				.put("/trip/{id}", 2)
//				.content(asJsonString(new Trip("Poland", "Warsaw", 1000)))
//				.contentType(MediaType.APPLICATION_JSON)
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(MockMvcResultMatchers.jsonPath("$.country").value("Poland"))
//				.andExpect(MockMvcResultMatchers.jsonPath("$.city").value("Warsaw"))
//				.andExpect(MockMvcResultMatchers.jsonPath("$.2000").value("1000"))
//				.andExpect(MockMvcResultMatchers.jsonPath("$.1").value("2"))
//				.andExpect(MockMvcResultMatchers.jsonPath("$.date").value("newDate"));
//	}

	@Test
	public void shouldReturnDeleteTrip() throws Exception{
		mvc.perform(MockMvcRequestBuilders.delete("id", 1))
				.andExpect(status().isAccepted());
	}


}
